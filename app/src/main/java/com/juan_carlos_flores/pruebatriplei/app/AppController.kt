package com.juan_carlos_flores.pruebatriplei.app

import android.app.Application
import android.content.Context
import com.juan_carlos_flores.pruebatriplei.api.TheStoreServiceClient.Companion.createClient
import com.juan_carlos_flores.pruebatriplei.api.TheStoreServiceClientI
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class AppController : Application() {
    private var saleService: TheStoreServiceClientI? = null
    private var scheduler: Scheduler? = null
    val storeService: TheStoreServiceClientI
        get() {
            if (saleService == null) {
                saleService = createClient()
            }
            return saleService!!
        }

    fun subscribeScheduler(): Scheduler {
        if (scheduler == null) {
            scheduler = Schedulers.io()
        }
        return scheduler!!
    }

    fun setUserService(saleService: TheStoreServiceClientI?) {
        this.saleService = saleService
    }

    fun setScheduler(scheduler: Scheduler?) {
        this.scheduler = scheduler
    }

    companion object {
        private operator fun get(context: Context): AppController {
            return context.applicationContext as AppController
        }

        fun create(context: Context): AppController {
            return get(context)
        }
    }
}