package com.juan_carlos_flores.pruebatriplei.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.juan_carlos_flores.pruebatriplei.R
import com.juan_carlos_flores.pruebatriplei.databinding.SalesItemViewBinding
import com.juan_carlos_flores.pruebatriplei.viewmodel.ItemSaleViewModel
import com.juan_carlos_flores.pruebatriplei.vo.Store

class SalesAdapter : RecyclerView.Adapter<SalesAdapter.Companion.SaleAdapterViewHolder> {

    private var saleList: ArrayList<Store>? = null

    constructor() {
        this.saleList = arrayListOf()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SaleAdapterViewHolder {
        var salesItemViewBinding = DataBindingUtil.inflate<SalesItemViewBinding>(
            LayoutInflater.from(
                parent.context
            ), R.layout.sales_item_view, parent, false
        )

        return SaleAdapterViewHolder(salesItemViewBinding)
    }

    override fun getItemCount(): Int {
        return saleList!!.size
    }

    fun setSaleList(saleList: ArrayList<Store>) {
        this.saleList = saleList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: SaleAdapterViewHolder, position: Int) {
        holder.bindSale(saleList!![position])
    }

    companion object {
        class SaleAdapterViewHolder : RecyclerView.ViewHolder {
            var mItem: SalesItemViewBinding? = null

            constructor(item: SalesItemViewBinding) : super(item.itemSale) {
                this.mItem = item
            }

            fun bindSale(store: Store) {
                if (mItem?.saleViewModel == null) {
                    mItem?.saleViewModel = ItemSaleViewModel(store, itemView.context)
                } else {
                    mItem?.saleViewModel?.store = store
                }
            }
        }
    }

}