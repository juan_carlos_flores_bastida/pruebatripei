package com.juan_carlos_flores.pruebatriplei.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juan_carlos_flores.pruebatriplei.R
import com.juan_carlos_flores.pruebatriplei.databinding.FragmentStoreListBinding
import com.juan_carlos_flores.pruebatriplei.ui.adapter.SalesAdapter
import com.juan_carlos_flores.pruebatriplei.viewmodel.SaleViewModel
import java.util.*

class StoreListFragment : Fragment(), Observer {

    var storeListBinding: FragmentStoreListBinding? = null
    var salesViewModel: SaleViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDataBinding()
        setUpListOfSalesView(storeListBinding?.recyclerviewSalesList)
        setUpObserver(salesViewModel)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun setUpObserver(observable: Observable?) {
        observable?.addObserver(this)
    }

    private fun setUpListOfSalesView(recyclerviewSalesList: RecyclerView?) {
        var adapter = SalesAdapter()
        recyclerviewSalesList?.adapter = adapter
        recyclerviewSalesList?.layoutManager = LinearLayoutManager(requireContext())

    }

    private fun initDataBinding() {
        storeListBinding = DataBindingUtil.setContentView(requireActivity(), R.layout.fragment_store_list)
        salesViewModel = SaleViewModel(requireContext())
        storeListBinding!!.saleViewModel = salesViewModel
    }

    /*override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store_list, container, false)
    }*/

    override fun update(o: Observable?, arg: Any?) {
        if (o is SaleViewModel) {
            val adapter: SalesAdapter = storeListBinding?.recyclerviewSalesList?.adapter as SalesAdapter
            val saleModel: SaleViewModel = o
            adapter.setSaleList(saleModel.getSaleList()!!)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        salesViewModel?.reset()
    }

}