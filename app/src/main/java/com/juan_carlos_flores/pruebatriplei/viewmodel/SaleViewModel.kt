package com.juan_carlos_flores.pruebatriplei.viewmodel

import android.content.Context
import android.view.View
import androidx.annotation.NonNull
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.juan_carlos_flores.pruebatriplei.R
import com.juan_carlos_flores.pruebatriplei.app.AppController
import com.juan_carlos_flores.pruebatriplei.vo.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import java.util.*
import kotlin.collections.ArrayList


class SaleViewModel(@NonNull val context: Context) : Observable() {

    var progressBar: ObservableInt? = null
    var recyclerView: ObservableInt? = null
    var messageLabel: ObservableInt? = null
    var errorMessage: ObservableField<String>? = null

    var _context: Context? = null

    private var listSale: ArrayList<Store>? = null
    private var compositeDisposable: CompositeDisposable? = CompositeDisposable()

    /*constructor(@NonNull context: Context) {
        this.listSale = arrayListOf()
        progressBar = ObservableInt(View.VISIBLE)
        recyclerView = ObservableInt(View.GONE)
        messageLabel = ObservableInt(View.GONE)
        errorMessage = ObservableField("Cargando Información por favor espere")
    }  */

    init {
        this._context = context
        this.listSale = arrayListOf()
        progressBar = ObservableInt(View.VISIBLE)
        recyclerView = ObservableInt(View.GONE)
        messageLabel = ObservableInt(View.GONE)
        errorMessage = ObservableField("Cargando Información por favor espere")
        initializeViews()
        fetchSaleList()
    }

    fun initializeViews() {
        messageLabel?.set(View.VISIBLE)
        recyclerView?.set(View.GONE)
        progressBar?.set(View.VISIBLE)
    }

    fun fetchSaleList() {
        var appController = AppController.create(_context!!)
        var saleService = appController.storeService

        var disposable: Disposable = saleService?.getStores(
            RequestGetStores(
                Usuario("11208"),
                Proyecto("137", 0)

            )
        )!!.subscribeOn(appController?.subscribeScheduler())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(Consumer<ResponseGetStores> { responseGetStores ->
                updateSalesList(responseGetStores.storeList)
                progressBar?.set(View.GONE)
                messageLabel?.set(View.GONE)
                recyclerView?.set(View.VISIBLE)
            }, Consumer<Throwable> {
                errorMessage?.set(_context?.getString(R.string.error_message_loading_users))
                progressBar?.set(View.GONE)
                messageLabel?.set(View.VISIBLE)
                recyclerView?.set(View.GONE)
            }
            )
        compositeDisposable?.add(disposable)
    }

    private fun updateSalesList(storeList: ArrayList<Store>) {
        listSale?.addAll(storeList)
        setChanged()
        notifyObservers()
    }

    fun getSaleList(): ArrayList<Store>? {
        return listSale
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable?.isDisposed!!) {
            compositeDisposable?.dispose()
        }
    }

    fun reset() {
        unSubscribeFromObservable()
        compositeDisposable = null
        _context = null
    }
}