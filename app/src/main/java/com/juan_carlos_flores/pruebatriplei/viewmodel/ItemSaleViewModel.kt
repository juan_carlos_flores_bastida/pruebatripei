package com.juan_carlos_flores.pruebatriplei.viewmodel

import android.content.Context
import android.view.View
import androidx.databinding.BaseObservable
import com.juan_carlos_flores.pruebatriplei.vo.Store

class ItemSaleViewModel(var store: Store, val context: Context): BaseObservable() {

    fun getCadena(): String {
        return "Cad: ${store.cadena}"
    }

    fun getDeterminante(): String {
        return "Det: ${store.determinante}"
    }

    fun getSucursal(): String {
        return "Suc: ${store.sucursal}"
    }

    fun getLatitud(): String {
        return "Lat: ${store.latitud}"
    }

    fun getLongitud(): String {
        return "Long: ${store.longitud}"
    }

    fun setSale(store: Store) {
        this.store = store
        notifyChange()
    }

    fun onItemClick(v: View) {
        println("Click en item sucursal: ${getSucursal()}")
        //context.startActivity(UserDetailActivity.fillDetail(v.context, user))
    }
}