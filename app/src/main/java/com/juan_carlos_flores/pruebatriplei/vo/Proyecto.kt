package com.juan_carlos_flores.pruebatriplei.vo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Proyecto(
    @SerializedName("Id")
    val id: String,
    @SerializedName("Ufechadescarga")
    val ufechadescarga: Int
): Parcelable