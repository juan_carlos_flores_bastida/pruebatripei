package com.juan_carlos_flores.pruebatriplei.vo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Store(
    @SerializedName("Cadena")
    val cadena: String,
    @SerializedName("determinante")
    val determinante: String,
    @SerializedName("Latitud")
    val latitud: Double,
    @SerializedName("Longitud")
    val longitud: Double,
    @SerializedName("Sucursal")
    val sucursal: String
): Parcelable