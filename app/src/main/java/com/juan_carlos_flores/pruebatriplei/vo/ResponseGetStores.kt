package com.juan_carlos_flores.pruebatriplei.vo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseGetStores(
    @SerializedName("getConjuntotiendasUsuarioResult")
    val storeList: ArrayList<Store>
): Parcelable