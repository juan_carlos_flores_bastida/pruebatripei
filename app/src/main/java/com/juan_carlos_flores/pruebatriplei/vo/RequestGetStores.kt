package com.juan_carlos_flores.pruebatriplei.vo


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestGetStores(
    @SerializedName("Usuario")
    val usuario: Usuario,
    @SerializedName("Proyecto")
    val proyecto: Proyecto
) : Parcelable