package com.juan_carlos_flores.pruebatriplei.repository

enum class Status {
    RUNNING,
    SUCCESS,
    FAILED,
    DISCONECTED
}

class NetworkState(val status: Status, val message: String) {

    companion object {
        val LOADED: NetworkState
        val LOADING: NetworkState
        val ERROR: NetworkState

        init {
            LOADED = NetworkState(status = Status.SUCCESS, message = "SUCCESS")
            LOADING = NetworkState(status = Status.RUNNING, message = "RUNNING")
            ERROR = NetworkState(status = Status.FAILED, message = "FAILED -- ¡Algo salio mal! --")
        }
    }
}