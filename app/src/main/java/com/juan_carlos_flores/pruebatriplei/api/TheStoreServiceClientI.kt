package com.juan_carlos_flores.pruebatriplei.api

import com.juan_carlos_flores.pruebatriplei.vo.RequestGetStores
import com.juan_carlos_flores.pruebatriplei.vo.ResponseGetStores
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface TheStoreServiceClientI {

    @POST("getConjuntotiendasUsuario")
    fun getStores(@Body requestGetStores: RequestGetStores): Observable<ResponseGetStores>
}